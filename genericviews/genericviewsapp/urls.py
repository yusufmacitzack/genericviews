
from django.urls import path, include
from .views import * 
urlpatterns = [
    path('list/', PublisherListView.as_view()),
    path('<pk>', PublisherDetailView.as_view()),
]
