from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, DetailView
from .models import *

class PublisherListView(ListView):
    model = Publisher
    template_name = "index.html"
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['book_list'] = Book.objects.all()
        context['author_list'] = Author.objects.all()
        print("CONTEXT", context)
        return context

class PublisherDetailView(DetailView):
    template_name = "index.html"
    queryset = Book.objects.all()
    context_object_name = 'book_list'